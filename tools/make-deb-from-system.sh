#!/usr/bin/env bash

echo "Start creating deb package..."

PROJECTNAME="gdm3-background"
DESCRIPTION="Program to change background image of your GDM3 login screen"
MAINTAINER="Lars A Landås"
VERSION="1.0"
ARCH="amd64"
DEB_DIR="_${PROJECTNAME}"

cd ..
mkdir -p ${DEB_DIR}/DEBIAN
mkdir -p ${DEB_DIR}/usr/local/bin
mkdir -p ${DEB_DIR}/usr/local/man/man1

cp /usr/local/bin/gdm3-background* ${DEB_DIR}/usr/local/bin/
cp /usr/local/man/man1/gdm3-background-update.1.gz ${DEB_DIR}/usr/local/man/man1/

find /usr/local/share/locale/ -type f 2>/dev/null | grep -i ${PROJECTNAME}.mo$ | xargs -i dirname {} | xargs -i mkdir -p ${DEB_DIR}/{}
find /usr/local/share/locale/ -type f 2>/dev/null | grep -i ${PROJECTNAME}.mo$ | xargs -i cp {} ./${DEB_DIR}{}

find /usr/share/icons/ -type f 2>/dev/null | grep -i ${PROJECTNAME}.png$ | xargs -i dirname {} | xargs -i mkdir -p ${DEB_DIR}/{}
find /usr/share/icons/ -type f 2>/dev/null | grep -i ${PROJECTNAME}.png$ | xargs -i cp {} ./${DEB_DIR}{}

find /usr/share/applications/ -type f 2>/dev/null | grep -i ${PROJECTNAME}.desktop$ | xargs -i dirname {} | xargs -i mkdir -p ${DEB_DIR}/{}
find /usr/share/applications/ -type f 2>/dev/null | grep -i ${PROJECTNAME}.desktop$ | xargs -i cp {} ./${DEB_DIR}{}

CONTROLFILE="Package: ${PROJECTNAME}
Version: ${VERSION}
Maintainer: ${MAINTAINER}
Architecture: ${ARCH}
Priorit: optional
License: GPL
Depends: libgtk-3-0 (>= 3.21.4), libcairo2 (>= 1.2.4), libglib2.0-0
Description: ${DESCRIPTION}"

echo "${CONTROLFILE}" > ${DEB_DIR}/DEBIAN/control
dpkg-deb --root-owner-group --build ${DEB_DIR}
mv ${DEB_DIR}.deb ${PROJECTNAME}_${VERSION}_${ARCH}.deb

echo "Completed!"

