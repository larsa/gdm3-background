#!/usr/bin/env bash

if [ ! -d "_bin" ]; then
    mkdir _bin
fi

if [ ! -d "_bin" ]; then
    echo "Error: could not make dir '_bin'"
fi

cd _bin
rm -R *
cmake ..
make
sudo make install
echo ""
echo ""
echo "To run program:"
echo "gdm3-background"
