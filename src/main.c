/*
 * main.c
 * This file is part of gdm3-background
 *
 * Copyright (C) 2018 - Lars A. Landås
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include <gdm3-background.h>
#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include <glib/gi18n.h>
#include <stdlib.h>
#include <locale.h>

GtkBuilder *gdm3_background_builder;

static void
gtk_alert_box_response(GtkDialog *dialog,
               gint       response_id,
               gpointer   userdata)
{
    gtk_widget_destroy((GtkWidget*)dialog);
}

static void
gtk_alert_box_with_title(gchar* msg, gchar* title)
{
    GtkWidget *dialog =
        gtk_dialog_new_with_buttons(
            title,
            (GtkWindow*)gtk_builder_get_object(gdm3_background_builder, "window"),
            GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
            _("_OK"),
            GTK_RESPONSE_OK,
            NULL);
    g_signal_connect(dialog, "response", G_CALLBACK(gtk_alert_box_response), NULL);

    GtkWidget *content = gtk_dialog_get_content_area((GtkDialog *)dialog);
    GtkWidget *label = gtk_label_new(msg);
    gtk_box_pack_start((GtkBox*)content,
                    label,
                    TRUE,
                    TRUE,
                    10);
    gtk_widget_show_all(content);
    gtk_dialog_run((GtkDialog *)dialog);
}

static void
gtk_alert_box(gchar* msg)
{
    gtk_alert_box_with_title(msg, _("Error"));
}

static void
gdm3_revert(GtkWidget *widget,gpointer data)
{
    gchar *std_output=NULL;
    gchar buffer[4096];
    GError *error = NULL;
    gint ret = 255;
    g_snprintf(buffer, 4096, "%s revert", GDM3_BACKGROUND_UPDATE_PATH);

    if(!g_spawn_command_line_sync(buffer,
                           &std_output,
                           NULL,
                           &ret,
                           &error))
    {
        gtk_alert_box(_("Could not launch helper program.\nInstallation probably broken."));
        g_debug("Could not launch helper program.");

        if(error != NULL) {
            g_debug("%s",error->message);
            g_clear_error(&error);
        }

        goto cleanup;
    }

    if(!g_spawn_check_exit_status(ret,
                           &error)) {
        g_snprintf(buffer, 4096, _("Helper program exited with errors (%d):\n%s"), error->code,std_output);
        g_debug("%s",buffer);
        gtk_alert_box(buffer);
        g_clear_error(&error);
        goto cleanup;
    }

    //Hide restore button
    GObject *gobject = gtk_builder_get_object(gdm3_background_builder, "revert");
    gtk_widget_hide((GtkWidget*)gobject);

    gtk_alert_box_with_title(_("System default are restored."),_("OK"));

cleanup:
    g_free(std_output);

}

static void
gdm3_background_save(GtkWidget *widget,gpointer data)
{
    GObject *gobject = gtk_builder_get_object(gdm3_background_builder, "light_chooser");
    gboolean is_light = gtk_toggle_button_get_active((GtkToggleButton*)gobject);

    GError *error = NULL;
    gint ret = 255;
    gchar *std_output=NULL;
    gchar buffer[4096];

    gobject = gtk_builder_get_object(gdm3_background_builder, "background_chooser");
    gchar *filename = gtk_file_chooser_get_filename((GtkFileChooser *)gobject);

    g_snprintf(buffer, 4096, "%s image %s %s", GDM3_BACKGROUND_UPDATE_PATH, (is_light?"--light":""), filename);
    g_free(filename);

    g_debug("Running command: %s",buffer);

    if(!g_spawn_command_line_sync(buffer,
                           &std_output,
                           NULL,
                           &ret,
                           &error))
    {
        gtk_alert_box(_("Could not launch helper program.\nInstallation probably broken."));
        g_debug("Could not launch helper program.");

        if(error != NULL) {
            g_debug("%s",error->message);
            g_clear_error(&error);
        }

        goto cleanup;
    }

    if(!g_spawn_check_exit_status(ret,
                           &error)) {
        g_snprintf(buffer, 4096, _("Helper program exited with errors (%d):\n%s"), error->code,std_output);
        g_debug("%s",buffer);
        gtk_alert_box(buffer);
        g_clear_error(&error);
        goto cleanup;
    }

    //Display save ok message + display revert button
    gobject = gtk_builder_get_object(gdm3_background_builder, "ok_message");
    gtk_widget_show_all((GtkWidget*)gobject);

    gobject = gtk_builder_get_object(gdm3_background_builder, "revert");
    gtk_widget_show_all((GtkWidget*)gobject);

    //hack to resize window to display all elements correct
    gobject = gtk_builder_get_object(gdm3_background_builder, "window");
    gtk_window_resize((GtkWindow*)gobject,1,1);

    g_debug("Process returned with %d\n",ret);

cleanup:
    g_free(std_output);
}

static void
gdm3_background_file_choosen(GtkWidget *widget,gpointer data) {
    GError *error = NULL;
    gchar *filename = gtk_file_chooser_get_filename((GtkFileChooser *)widget);

    GObject *gobject = gtk_builder_get_object(gdm3_background_builder, "ok_message");
    gtk_widget_hide((GtkWidget*)gobject);

    GFile *file = g_file_new_for_path(filename);

    GFileInputStream * filestream =  g_file_read(file,
             NULL,
             &error);

    if(error!=NULL) {
        g_printerr("Error loading file: %s", error->message);
        gtk_alert_box(_("Error loading selected file"));
        g_clear_error(&error);
        goto freeup;
    }

   guchar file_buffer[4096];

    gsize file_buffer_length = g_input_stream_read((GInputStream*)filestream,
                     file_buffer,
                     4096,
                     NULL,
                     &error);

    if(error!=NULL) {
        g_printerr("Error reading file: %s\n", error->message);
        gtk_alert_box(_("Could not read selected file"));
        g_clear_error(&error);
        goto freeup;
    }
    gboolean content_result = TRUE;
    gchar *file_content_type = g_content_type_guess(filename,
                      file_buffer,
                      file_buffer_length,
                      &content_result);

    g_debug("File type of %s file: %s",filename,file_content_type);

    gobject = gtk_builder_get_object(gdm3_background_builder, "error_message");

    /* Only Ubuntu supported, so no need to check for real mime type */
    if(g_strcmp0(file_content_type,"image/jpeg")!=0 && g_strcmp0(file_content_type,"image/png")!=0) {
        gtk_label_set_text((GtkLabel*)gobject,_("File type choosen is not supported."));
        gtk_widget_show_all((GtkWidget*)gobject);
        gobject = gtk_builder_get_object(gdm3_background_builder, "save");
        gtk_widget_set_sensitive((GtkWidget *)gobject, FALSE);
        g_free(file_content_type);
        g_debug("Unsupported file type");
        //hack to resize window to display all elements correct
        gobject = gtk_builder_get_object(gdm3_background_builder, "window");
        gtk_window_resize((GtkWindow*)gobject,1,1);
        goto freeup;
    } else {
        gtk_widget_hide((GtkWidget*)gobject);
        gobject = gtk_builder_get_object(gdm3_background_builder, "save");
        g_free(file_content_type);
        gtk_widget_set_sensitive((GtkWidget *)gobject, TRUE);
    }

freeup:
    g_free(filename);
    g_object_unref(file);
    g_object_unref(filestream);
}


int
main(int argc, char *argv[])
{
    GObject *window;
    GObject *gobject;
    GError *error = NULL;
    gboolean system_is_supported = TRUE;
    gchar *filename = NULL;
    gchar info_label[1024];

    gtk_init(&argc, &argv);

    setlocale(LC_ALL, "");
    bindtextdomain("gdm3-background", "/usr/share/locale");
    bindtextdomain("gdm3-background", GDM3_LOCALE_DIR);
    textdomain("gdm3-background");

    /* Construct a GtkBuilder instance and load our UI description */
    gdm3_background_builder = gtk_builder_new();
    if(gtk_builder_add_from_resource(gdm3_background_builder, "/app/gdm3-background.ui", &error) == 0)
    {
        gtk_alert_box(_("Fatal error: Could not load UI file"));
        g_printerr("Error loading file: %s\n", error->message);
        g_clear_error(&error);
        return 1;
    }

    GtkCssProvider * css = gtk_css_provider_new();
    gtk_css_provider_load_from_resource(css, "/app/gdm3-background.css");

    if(error != NULL) {
        g_printerr("Error loading css file: %s\n", error->message);
        g_clear_error(&error);
    } else {
        gtk_style_context_add_provider_for_screen(
                gdk_screen_get_default(),
                GTK_STYLE_PROVIDER(css),
                GTK_STYLE_PROVIDER_PRIORITY_USER);
    }

    gobject = gtk_builder_get_object(gdm3_background_builder, "save");
    gtk_widget_set_sensitive((GtkWidget*)gobject,FALSE);

    if(!system_is_supported) {
        gobject = gtk_builder_get_object(gdm3_background_builder, "info_message");
        gtk_widget_hide((GtkWidget*)gobject);
        gobject = gtk_builder_get_object(gdm3_background_builder, "save");
        gtk_widget_hide((GtkWidget*)gobject);
        gobject = gtk_builder_get_object(gdm3_background_builder, "chooser_grid");
        gtk_widget_hide((GtkWidget*)gobject);
        gobject = gtk_builder_get_object(gdm3_background_builder, "error_message");
        gtk_widget_show_all((GtkWidget*)gobject);
        gobject = gtk_builder_get_object(gdm3_background_builder, "cancel");
        gtk_button_set_label((GtkButton*)gobject, "_OK");
        //hack to resize window to display all elements correct
        gobject = gtk_builder_get_object(gdm3_background_builder, "window");
        gtk_window_resize((GtkWindow*)gobject,1,1);
    }

    //should we display restore button
    GFile *css_link_file = g_file_new_for_path("/etc/alternatives/gdm3.css");
    GFileInfo *css_link = g_file_query_info(css_link_file,
                                            "standard::symlink-target",
                                            G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                                            NULL,
                                            &error);

    if(error != NULL) {
        g_printerr("Error reading gdm3.css symlink info: %s\n", error->message);
        g_clear_error(&error);
    } else {
        if(g_strcmp0(g_file_info_get_symlink_target(css_link),
           "/usr/share/gnome-shell/theme/gdm3-custom.css") == 0) {
            //Display revert object as it points to our css file
            gobject = gtk_builder_get_object(gdm3_background_builder, "revert");
            gtk_widget_show_all((GtkWidget*)gobject);
        }

        g_object_unref(css_link);
        g_object_unref(css_link_file);
    }

    gobject = gtk_builder_get_object(gdm3_background_builder, "background_chooser");
    GtkFileFilter* filter = gtk_file_chooser_get_filter((GtkFileChooser*)gobject);

    /* Connect signal handlers to the constructed widgets. */
    window = gtk_builder_get_object(gdm3_background_builder, "window");
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    gobject = gtk_builder_get_object(gdm3_background_builder, "revert");
    g_signal_connect(gobject, "clicked", G_CALLBACK(gdm3_revert), NULL);

    gobject = gtk_builder_get_object(gdm3_background_builder, "cancel");
    g_signal_connect(gobject, "clicked", G_CALLBACK(gtk_main_quit), NULL);

    gobject = gtk_builder_get_object(gdm3_background_builder, "save");
    g_signal_connect(gobject, "clicked", G_CALLBACK(gdm3_background_save), NULL);

    gobject = gtk_builder_get_object(gdm3_background_builder, "background_chooser");
    g_signal_connect(gobject, "file-set", G_CALLBACK(gdm3_background_file_choosen), NULL);

    gtk_main();

    return 0;
}
