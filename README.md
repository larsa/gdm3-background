# GDM3 BACKGROUND

![alt Screenshot](https://gitlab.gnome.org/larsa/gdm3-background/raw/master/data/screenshot.png)

A program for people who wants to customize background image or color for the GDM3 login screen.  
License: GPLv2

**The gdm3-background consists of two programs:**
 - gdm3-background-update (bash) - command line tool for chaning login screen background image.
 - gdm3-background (C) - A GTK3 graphical user interface around the command line tool.

## Compatibility
This program only works for Ubuntu 18.04 right now. It might work for Ubuntu 17.10 (not tested).  
I will probably make a version for Fedora when I have time.

The difference between how Fedora and Ubuntu now handles the CSS file for GDM, is too big to only tweek the comannd line tool.

## Dependencies
 - polkit - Used in the command line tool to get root access, using `pkexec` (asks for password).
 - gtk3 - For the graphical user interface
 - update-alternatives - Ubuntu implementation of changing CSS of GDM3

## Install
This program will only work on Ubuntu 18.04+, and maybe Ubuntu 17.10 (not tested).

It also might work on other distribution derived from Ubuntu 18.04, as an Ubuntu contributor said their GDM3 CSS implementation was implemented for other Ubuntu derived distributions to easy style their loginscreen. (see references)

### Download deb package for Ubuntu 18.04
https://gitlab.gnome.org/larsa/gdm3-background/tree/master/ubuntu-package

### Download files (GIT) 
`git clone git@gitlab.gnome.org:larsa/gdm3-background.git`  
`cd gdm3-background`

### Compile (debug version, no install)
From withing *gdm3-background* directory:  
`./make-debug.sh`  
`cd _bin && ./run.sh`

run.sh will setup enviroment variables to run graphical user interface from user folder.  
You will also find the command line tool `gdm3-background-updater` in *_bin* folder.

### Compile (release version, install system wide)
This will install the application, and you will get a nice icon in the application menu.  
From withing *gdm3-background* directory (will ask for root password):  
`./make-sudo-install.sh`

If sucessfull installed you can run the two commands from anywhere in your terminal, or launch the graphical user interface from the appication menu.

`gdm3-background` - Graphical user interface  
`gdm3-background-update` - Command line tool

## The command line tool
**Includes the following three commands:**

`gdm3-background-update image [--light]  <absolute_path_to_image>` -- change background image  
`gdm3-background-update color [--light] '<color_value>'` -- change background color  
`gdm3-background-update revert` -- revert to distribution default settings

Use `gdm3-background-update help` for more information of each command.

### Use the command line interface in you own project
If you use the command line tool in your project, I would be happy if you left a notice in issues, with a link to you project:  
https://gitlab.gnome.org/larsa/gdm3-background/issues

The command line tool is just a standard bash script, with very few dependencies that normally should be installed in standard Linux distributions. Right now the tool only works for Ubuntu 18.04 (maybe 17.10).

If you onlye want the command line tool you can actually just copy it from the repository without futher actions:  
https://gitlab.gnome.org/larsa/gdm3-background/blob/master/bin/gdm3-background-update

**The command line tool is doing roughly this to change background image (with root access from pkexec):**
1) Check that image is either JPEG og PNG.
2) Copy image to */usr/share/backgrounds/*.
3) Read content of current CSS file.
4) Copy the content, append custom css and write new css file to:  
   */usr/share/gnome-shell/theme/gdm3-custom.css*
5) Install new css-file with `update-alternatives`

See the man page for information about exit values.

## References used to create this program
 - http://ubuntuhandbook.org/index.php/2017/10/change-login-screen-background-ubuntu-17-10/
 - https://discourse.ubuntu.com/t/changing-the-grub-splash-and-gdm-lock-screen-color/7516
 - https://didrocks.fr/2017/09/11/ubuntu-gnome-shell-in-artful-day-11/
 - GTK3 manual
 - Stack Exchange
