#!/usr/bin/env bash

while read lang; do
  if [ -f "${lang}.po" ]; then
    echo -n "Updating $lang"
    msgmerge -U "${lang}.po" messages.po
  fi
done <LINGUAS
