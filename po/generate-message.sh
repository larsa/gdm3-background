#!/usr/bin/env bash
xgettext -j --files-from=POTFILES.in --directory=../ --from-code=UTF-8 --keyword=_ --keyword=N_ --keyword=C_:1c,2 --keyword=NC_:1c,2 --keyword=g_dngettext:2,3 --add-comments
