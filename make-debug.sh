#!/usr/bin/env bash

if [ ! -d "_bin" ]; then
    mkdir _bin
fi

if [ ! -d "_bin" ]; then
    echo "Error: could not make dir '_bin'"
fi

cd _bin
rm -R *
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
echo ""
echo ""
echo "To start program type:"
echo "cd _bin && ./run.sh"
